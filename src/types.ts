export type Post = {
  id: number;
  content: string;
  updatedAt: string;
  createdAt: string;
};

export type CreatePostDto = unknown;

export type UpdatePostDto = unknown;
